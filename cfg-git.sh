#!/bin/sh

# Copy .configs to git dotconfig
cp ~/.config/kwinrc ~/Documents/Projects/dotfiles/.config/kwinrc
cp ~/.zshrc ~/Documents/Projects/dotfiles/.zshrc
cp ~/.vimrc ~/Documents/Projects/dotfiles/.vimrc
cp ~/.oh-my-zsh/custom/greet.sh ~/Documents/Projects/dotfiles/.oh-my-zsh/custom/greet.sh
cp ~/.oh-my-zsh/custom/keys.sh ~/Documents/Projects/dotfiles/.oh-my-zsh/custom/keys.sh
cp ~/.local/share/konsole/zsh.profile ~/Documents/Projects/dotfiles/.local/share/konsole/zsh.profile

